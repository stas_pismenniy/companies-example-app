import React, { useState, useEffect } from 'react'
import Home from './components/Home'
import Bakery from './components/Bakery'
import Suppliers from './components/Suppliers'
import NavBar from './components/Navbar'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

function App() {
  return (
    <Router>
      <div className='app-companies'>
        <NavBar/>
        <Route exact path='/' component={Home} />
        <Route path='/bakery/:id' component={Bakery} />
        <Route path='/suppliers/:id' component={Suppliers} />
      </div>
    </Router>
  )
}

export default App
