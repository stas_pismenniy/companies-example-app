import React from 'react'
const Banner = ({ title, pageClass }) => {
  return (
    <div className={`banner center ${pageClass}`}>
      <h1>{title}</h1>
    </div>
  )
}

export default Banner
