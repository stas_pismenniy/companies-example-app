import React, { Component } from 'react'
import { connect } from 'react-redux'
import Banner from '../components/Banner'
import Description from '../components/Description'
import RssFeed from '../components/RssFeed'
class Home extends Component {
  render () {
    const url = 'http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=0d9c4af9c5ac41909c93578ab7e6f696'
    const description = '' +
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. ' +
      'Accusamus dolores eos minima nobis officiis quis quod temporibus tenetur! ' +
      'Autem doloribus unde velit. Aliquid ducimus incidunt inventore neque non ' +
      'quidem reiciendis.'
    return (
      <div className='home'>
        <Banner title='Our Cafe' pageClass='home' />
        <div className="container">
          <Description description={description} />
          <RssFeed url={url}/>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  console.log(state)
  return {
    companies: state.data
  }
}

export default connect(mapStateToProps)(Home)
