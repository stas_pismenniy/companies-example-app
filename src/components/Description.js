import React from 'react'
const Description = ({ description }) => {
  return (
    <div className='description center'>
      <p>{description}</p>
    </div>
  )
}

export default Description
