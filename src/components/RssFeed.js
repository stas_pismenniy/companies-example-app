import React, {useState, useEffect} from 'react'
import {getNews} from '../utils'
import Article from './Article'


const RssFeed = ({url}) => {
  const [data, setData] = useState([])
  function fetchNews () {
    getNews(url)
      .then(news => {
        console.log('news', news)
        setData(news)
      })
      .catch(e => console.log('error', e))
  }
  useEffect(() => {
    // Update the document title using the browser API
    fetchNews()
    console.log('data123', data)
  }, [])
  const postList = data.length ?
    (data.map((post, index) => {
      return (
        <div key={index}>
          <Article post={post} index={index.toString()}/>
        </div>
        )
    })) : ''
  return (
    <div className="container home">
      <h4 className="center">Latest news</h4>
      {postList}
    </div>
  )
}

export default RssFeed
