import React from 'react'
import {Link, NavLink, withRouter} from 'react-router-dom'
const NavBar = () => {
  return (
    <nav className='nav-wrapper red darken-3'>
      <div className="container">
        <a href="/" className="brand-logo">Companies</a>
        <ul className='right'>
          <li><Link to='/'>Home</Link></li>
          <li><NavLink to='/bakery/1'>Bakery</NavLink></li>
          <li><NavLink to='/suppliers/2'>Suppliers</NavLink></li>
        </ul>
      </div>
    </nav>
  )
}

export default withRouter(NavBar)
