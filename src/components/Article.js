import React from 'react'

const Article = ({post, index}) => {
  console.log('index', index)
  return (
    <div className='post card'>
      <div className="card-image waves-effect waves-block waves-light">
        <img className="activator" src={post.urlToImage} alt={post.title}/>
      </div>
      <div className="card-content">
          <span className="card-title activator grey-text text-darken-4">{post.title}</span>
        <p><a href={post.url} target='_blank'>This is a link</a></p>
      </div>
    </div>
  )
}

export default Article
