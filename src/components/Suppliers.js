import React, {Component} from 'react'
import {connect} from 'react-redux'
import Banner from './Banner'
import Description from './Description'
import RssFeed from "./RssFeed";

class Suppliers extends Component {
  render() {
    const url = 'http://newsapi.org/v2/top-headlines?country=us&category=technology&apiKey=0d9c4af9c5ac41909c93578ab7e6f696'
    const company = this.props.company ? (
      <div className='company suppliers'>
        <Banner title={this.props.company.name} />
        <div className="container">
          <Description description={this.props.company.desc} />
          <RssFeed url={url} />
        </div>
      </div>
    ) : (
      <div className='center'>Loading Company</div>
    )

    return (
      <>
        {company}
      </>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let id = ownProps.match.params.id
  return {
    company: state.data.find(company => company.id === id)
  }
}

export default connect(mapStateToProps)(Suppliers)
